<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('site')
                ->index()
            ;

            $table->foreign('site')
                ->references('id')
                ->on('sites')
            ;

            $table->integer('month');

            $table->float('visible');
            $table->float('keywords');
            $table->float('traff');
            $table->float('visible_dynamic');
            $table->float('keywords_dynamic');
            $table->float('traff_dynamic');
            $table->float('ads_dynamic');
            $table->float('new_keywords');
            $table->float('out_keywords');
            $table->float('rised_keywords');
            $table->float('down_keywords');
            $table->float('ad_keywords');
            $table->float('ads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics');
    }
}
