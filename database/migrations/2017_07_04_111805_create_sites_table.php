<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('engine');
            $table->string('region');
            $table->unsignedInteger('responsible_seo')
                ->index()
            ;

            $table->unsignedInteger('responsible_wm')
                ->index()
            ;

            $table->foreign('responsible_seo')
                ->references('id')
                ->on('responsibles')
            ;
            $table->foreign('responsible_wm')
                ->references('id')
                ->on('responsibles')
            ;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
