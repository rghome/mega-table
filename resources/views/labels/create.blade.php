<form>
    {{--{{dump($label)}}--}}
    @if (isset($label) && $label->id)
        <input type="hidden" required class="form-control" name="id" id="id" value="{{$label->id}}">
    @endif

    <div class="form-group">
        <label for="name" class="control-label">Name:</label>
        <input type="text" required class="form-control" name="name" id="name" @if (isset($label) && $label->name) value="{{$label->name}}" @endif>
    </div>
</form>