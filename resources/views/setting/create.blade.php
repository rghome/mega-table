<form>
    {{--{{dump($role)}}--}}
    @if (isset($settings) && $settings->id)
        <input type="hidden" required class="form-control" name="id" id="id" value="{{$settings->id}}">
    @endif

    <div class="form-group">
        <label for="name" class="control-label">Ключ:</label>
        <input type="text" required class="form-control" name="key" id="key" @if (isset($settings) && $settings->key) value="{{$settings->key}}" @endif >
    </div>
    <div class="form-group">
        <label for="name" class="control-label">Значение:</label>
        <input type="text" required class="form-control" name="value" id="value" @if (isset($settings) && $settings->value) value="{{$settings->value}}" @endif >
    </div>
</form>