<form>
    @if (isset($sites) && $sites->id)
        <input type="hidden" required class="form-control" name="id" id="id" value="{{$sites->id}}">
    @endif

    <div class="form-group">
        <label for="url" class="control-label">URL:</label>
        <input type="text" required class="form-control" name="url" id="url" placeholder="site.com" @if (isset($sites) && $sites->url) value="{{$sites->url}}" @endif>
    </div>
    <div class="form-group">
        <label for="label" class="control-label">Группа:</label><br>
        <select class="selectpicker" name="label" id="label">
            @foreach($labels as $label)
                <option
                    @if (isset($sites) && $sites->label == $label->id) selected @endif
                    @if ((isset($sites) && $sites->mirror !== null) && (3 !== $label->id)) disabled @endif
                    value="{{$label->id}}">
                    {{$label->name}}
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="engine" class="control-label">Engine:</label><br>
        <select class="selectpicker" name="engine" id="engine">
            <option value="0">Выбрать:</option>
            <option @if (isset($sites) && $sites->engine == 'google') selected @endif value="google">Google</option>
            <option @if (isset($sites) && $sites->engine == 'yandex') selected @endif value="yandex">Yandex</option>
        </select>
    </div>
    <div class="form-group">
        <label for="region" class="control-label">Region:</label>
        <select class="selectpicker" name="region" id="region">
            <option value="0">Выбрать:</option>
            @foreach($regions as $key => $region)
                <option @if (isset($sites) && $sites->region == $key) selected @endif value="{{$key}}">{{$region}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="responsible_seo" class="control-label">Ответственный SEO:</label><br>
        <select class="selectpicker" name="responsible_seo" id="responsible_seo">
            @foreach($responsibleSeo->users as $itemSeo)
                <option @if (isset($sites) && $sites->responsible_seo == $itemSeo->id) selected @endif value="{{$itemSeo->id}}">{{$itemSeo->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="responsible_wm" class="control-label">Ответственный WM:</label><br>
        <select class="selectpicker" name="responsible_wm" id="responsible_wm">
            @foreach($responsibleWM->users as $itemWM)
                <option @if (isset($sites) && $sites->responsible_wm == $itemWM->id) selected @endif value="{{$itemWM->id}}">{{$itemWM->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="enableMirror" class="control-label">Старое зеркало:</label><br>
        <div class="material-switch">
            <input id="enableMirror" type="checkbox" @if ((isset($sites) && $sites->mirror !== null)) checked="checked" @endif/>
            <label for="enableMirror" class="label-primary"></label>
        </div>
    </div>
    <div class="form-group" id="mirror-wrapper" @if ((isset($sites) && $sites->mirror === null)) style="display: none;" @endif>
        <label for="mirror" class="control-label">Выбрать зеркало:</label><br>
        <select class="selectpicker" name="mirror" id="mirror">
            <option value="0">Выбрать:</option>
            @foreach($sitesAll as $site)
                <option @if (isset($sites) && $sites->mirror == $site->id) selected @endif value="{{$site->id}}">{{$site->url}}</option>
            @endforeach
        </select>
    </div>
</form>