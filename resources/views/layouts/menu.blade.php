<div class="panel-heading">Menu</div>

<div class="panel-body">
    <ul class="nav">
        <li {{ (Route::current()->getName() === 'statistic' ? 'class=active' : '') }}>
            {{--<a href="{{ url('/') }}"></a>--}}
            <router-link to="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Статистика</router-link>
        </li>
        <li {{ (Route::current()->getName() === 'sites' ? 'class=active' : '') }}>
            {{--<a href="{{ url('sites') }}"><span class="glyphicon glyphicon-globe"></span> Сайты</a>--}}
            <router-link to="/sites"><span class="glyphicon glyphicon-globe"></span> Сайты</router-link>
        </li>
        @if (Auth::user()->hasRole('admin'))
            <li {{ (Route::current()->getName() === 'hold' ? 'class=active' : '') }}>
                {{--<a href="{{ url('sites') }}"><span class="glyphicon glyphicon-globe"></span> Сайты</a>--}}
                <router-link to="/hold"><span class="glyphicon glyphicon-alert"></span> Холд</router-link>
            </li>
            <li {{ (Route::current()->getName() === 'labels' ? 'class=active' : '') }}>
                {{--<a href="{{ url('labels') }}"><span class="glyphicon glyphicon-tag"></span> Группы</a>--}}
                <router-link to="/labels"><span class="glyphicon glyphicon-tag"></span> Метки</router-link>
            </li>
            <li {{ (Route::current()->getName() === 'user' ? 'class=active' : '') }}>
                {{--<a href="{{ url('responsible') }}"><span class="glyphicon glyphicon-user"></span> Ответственные</a>--}}
                <router-link to="/user"><span class="glyphicon glyphicon-user"></span> Пользователи</router-link>
            </li>
            <li {{ (Route::current()->getName() === 'role' ? 'class=active' : '') }}>
                {{--<a href="{{ url('responsible') }}"><span class="glyphicon glyphicon-user"></span> Ответственные</a>--}}
                <router-link to="/role"><span class="glyphicon glyphicon-folder-close"></span> Группы</router-link>
            </li>
            <li {{ (Route::current()->getName() === 'setting' ? 'class=active' : '') }}>
                {{--<a href="{{ url('responsible') }}"><span class="glyphicon glyphicon-user"></span> Ответственные</a>--}}
                <router-link to="/setting"><span class="glyphicon glyphicon-cog"></span> Настройки</router-link>
            </li>
        @endif
    </ul>
</div>