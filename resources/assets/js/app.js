
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('chart.js');
require('hchs-vue-charts');

import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'

window.Vue = require('vue');

Vue.use(VueCharts);

var VueRouter = require('vue-router/dist/vue-router.js');
Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const User          = Vue.component('user-index', require('./components/user/index.vue'));
const Sites         = Vue.component('sites-index', require('./components/sites/index.vue'));
const Hold          = Vue.component('hold-index', require('./components/hold/index.vue'));
const Setting       = Vue.component('setting-index', require('./components/setting/index.vue'));
const SitesEntry    = Vue.component('sites-entry', require('./components/sites/entry.vue'));
const Labels        = Vue.component('labels-index', require('./components/labels/index.vue'));
const Statistic     = Vue.component('statistic-index', require('./components/statistic/index.vue'));
const Role          = Vue.component('role-index', require('./components/roles/index.vue'));

Vue.component('data-filter', require('./components/filter.vue'));

const routes = [
    { path: '/', name: 'Statistic', component: Statistic },
    { path: '/labels', name: 'Labels', component: Labels },
    { path: '/sites', name: 'Sites', component: Sites },
    { path: '/hold', name: 'Hold', component: Hold },
    { path: '/site/:id', name: 'SitesEntry', component: SitesEntry },
    { path: '/user', name: 'User', component: User },
    { path: '/role', name: 'Role', component: Role },
    { path: '/setting', name: 'Setting', component: Setting },
];

const router = new VueRouter({
    routes, // short for routes: routes
    mode: 'history'
});


// If using mini-toastr, provide additional configuration
const toastTypes = {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
};

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
    return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
}

Vue.use(VueNotifications, options);

const app = new Vue({
    router
}).$mount('#app')
