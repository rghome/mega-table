<?php

namespace App\Models;

use App\Services\SeprSearchService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Sites
 * @package App\Models
 */
class Sites extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'sites';

    /**
     * @var array
     */
    protected $fillable = [
        'url', 'label', 'engine', 'region', 'responsible_seo', 'responsible_wm', 'created_at', 'updated_at', 'mirror',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsibleSeo()
    {
        return $this->belongsTo('App\User', 'responsible_seo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsibleWM()
    {
        return $this->belongsTo('App\User', 'responsible_wm');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statistics()
    {
        return $this->hasMany('App\Models\Statistics', 'site');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function siteLabel()
    {
        return $this->belongsTo('App\Models\Label', 'label');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function isMirror()
    {
        return $this->belongsTo('App\Models\Sites', 'mirror');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getMirrors()
    {
        return $this->hasMany('App\Models\Sites', 'mirror');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getLinks()
    {
        return $this->hasMany('App\Models\Links', 'site_id');
    }

    /**
     * Delete relations
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($sites) { // before delete() method call this
            $sites->statistics()->delete();
            $sites->getMirrors()->delete();
            $sites->getLinks()->delete();
            // do the rest of the cleanup...
        });
    }
}
