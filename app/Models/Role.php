<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/19/17
 * Time: 7:23 PM
 */

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

/**
 * Class Role
 * @package App\Models
 */
class Role extends EntrustRole
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'display_name', 'description', 'created_at', 'updated_at'
    ];
}