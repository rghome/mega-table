<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Responsible
 * @package App\Models
 */
class Responsible extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'responsibles';

    /**
     * @var array
     */
    protected $fillable = [
        'username', 'firstname', 'lastname', 'role', 'created_at', 'updated_at'
    ];
}
