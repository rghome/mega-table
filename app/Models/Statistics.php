<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Statistics
 * @package App\Models
 */
class Statistics extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'statistics';

    /**
     * @var array
     */
    protected $fillable = [
        'site', 'month', 'visible', 'keywords', 'traff', 'visible_dynamic', 'keywords_dynamic', 'traff_dynamic',
        'ads_dynamic', 'new_keywords', 'out_keywords', 'rised_keywords', 'down_keywords', 'ad_keywords', 'ads',
        'top1', 'top23', 'top45', 'top610', 'top1120', 'top2150', 'top3', 'top5', 'top20', 'top50', 'top10',
        'links_domain', 'links_pages', 'links_index', 'engine',
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsibleWM()
    {
        return $this->belongsTo('App\Models\Sites', 'id');
    }
}
