<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/7/17
 * Time: 7:11 PM
 */

namespace App\Http\Controllers\Helpers;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class SortingTrait
 * @package App\Http\Controllers\Helpers
 */
trait SortingTrait
{
    /**
     * @param Collection $collection
     * @return array
     */
    public function sortingLabels(Collection $collection): array
    {
        $tmp = [];
        $tmpLabels = [];
        foreach ($collection as $site) {
            if (!isset($tmpLabels[$site->siteLabel->name])) {
                $tmpLabels[$site->siteLabel->name] = true;

                $tmp[] = [
                    'label_name' => $site->siteLabel->name
                ];
            }

            $tmp[] = $site;
        }

        return $tmp;
    }
}