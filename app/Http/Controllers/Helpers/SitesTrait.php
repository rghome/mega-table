<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/5/17
 * Time: 2:11 PM
 */

namespace App\Http\Controllers\Helpers;

use App\Http\Controllers\HoldController;
use App\Models\Responsible;
use App\Models\Sites;
use App\Services\MegaIndexService;
use App\Services\SeprSearchService;
use App\Services\StatisticService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Routing\Route;

/**
 * Class SitesTrait
 * @package App\Http\Controllers\Helpers
 */
trait SitesTrait
{
    /**
     * @var StatisticService
     */
    private $statisticService;

    /**
     * @var array
     */
    private $monthsStart;

    /**
     * StatisticController constructor.
     */
    public function __construct()
    {
//        self::__construct();

        $this->middleware('auth');

        $this->statisticService = new StatisticService();

        $this->monthsStart = $this->statisticService->getMonthsStart();
    }

    /**
     * @param Route $route
     * @return JsonResponse
     */
    public function getSitesList(Route $route)
    {
        $isHold = ($route->getController() instanceof HoldController);

        $sites = \Cache::rememberForever('sites-list-without-stat', function () use ($isHold) {
            $sites = Sites::with('responsibleSeo')
                ->with('responsibleWM')
                ->with('siteLabel')
                ->with('isMirror')
                ->where('enabled', !$isHold)
                ->orderBy('label', 'asc')
//            ->with('statistics')
                ->get()
            ;

            return $this->sortingLabels($sites);
        });

        $regions = SeprSearchService::REGIONS + MegaIndexService::REGIONS;

        return response()->json(compact('sites', 'regions'));
    }
}