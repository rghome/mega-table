<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/5/17
 * Time: 2:20 PM
 */

namespace App\Http\Controllers\Helpers;

use App\Exceptions\StatisticServiceException;
use App\Models\Label;
use App\Models\Responsible;
use App\Models\Role;
use App\Models\Sites;
use App\Services\StatisticService;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

/**
 * Class FilterTrait
 * @package App\Http\Controllers\Helpers
 */
trait FilterTrait
{
    use SortingTrait;

    /**
     * @return JsonResponse
     */
    public function filterGetData()
    {
        $responsibleSeo = Role::where('id', 8)->with('users')->first();
        $responsibleWM = Role::where('id', 7)->with('users')->first();
        $labels = Label::all();

        return response()->json([
            'responsible_seo' => $responsibleSeo,
            'responsible_wm' => $responsibleWM,
            'labels' => $labels
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function filter(Request $request)
    {
//        try {
//            (new StatisticService())->updateStatistic();
//        } catch (StatisticServiceException $e) {}
        $model = $this->doFilter($request);


        $monthsStart = $this->monthsStart;

        return response()->json(compact('model', 'monthsStart'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function doFilter(Request $request)
    {
        $user = Auth::user();
        $rawRequest = $request->all();
        $filtered = false;
        $statisticsEngine = null;

        if (isset($rawRequest['statistics_engine']) && $rawRequest['statistics_engine']) {
            $statisticsEngine = $rawRequest['statistics_engine'];
            unset($rawRequest['statistics_engine']);
        }

        $model = Sites::class;
        foreach ($rawRequest as $key => $value) {
            if (!$value) continue;

            $filtered = true;
            if (is_object($model)) {
                $model = $model->where($key, $value);
            } else {
                $model = $model::where($key, $value);
            }
        }

        if ($filtered) {
            $model = $model->with('responsibleSeo');
        } else {
            $model = $model::with('responsibleSeo');
        }

        if ($statisticsEngine) {
            $model = $model->with([
                'statistics' => function($query) use ($statisticsEngine) {
                    $query->where('engine', '=', $statisticsEngine);
                },
            ]);
        } else {
            $model = $model->with([
                'statistics' => function($query) {
                    /** @var HasMany $query */

                    $site = Sites::where('id', $query->getResults()->get(0)->site)->first();

                    $query->where('engine', '=', $site->engine);
                },
            ]);
        }

        if ($user->hasRole('webmaster')) {
            $model = $model->where('responsible_wm', $user->id);
        } else if ($user->hasRole('seo')) {
            $model = $model->where('responsible_seo', $user->id);
        }

        $model = $model->with('responsibleWM')
            ->with('siteLabel')
            ->with('isMirror')
            ->where('enabled', '1')
            ->orderBy('label', 'asc')
            ->get()
        ;

        return $this->sortingLabels($model);
    }
}