<?php

namespace App\Http\Controllers;

use App\Models\Label;
use App\Models\Responsible;
use App\Models\Sites;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class LabelController
 * @package App\Http\Controllers
 */
class LabelController extends Controller
{
    /**
     * LabelController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index()
    {
        return view('labels.index');
    }

    /**
     * @return JsonResponse
     */
    public function getLabelsList()
    {
        $labels = Label::with('sites')->get();

        return response()->json(compact('labels'));
    }

    /**
     * @param Label|null $label
     * @return View
     */
    public function getCreateForm(Label $label = null)
    {
        return view('labels.create', compact('label'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $labels = Label::create($request->all());

        $labels = Label::where('id', $labels->id)
            ->with('sites')
            ->first()
        ;

        return response()->json(compact('labels'));
    }

    /**
     * @param Request $request
     * @param Label $labels
     * @return JsonResponse
     */
    public function update(Request $request, Label $labels)
    {
//        $labels->update();

        $data = $request->all();

        $labels = Label::where('id', $data['id'])
            ->with('sites')
            ->first()
        ;

        $labels->name = $data['name'];
        $labels->save();

        return response()->json(compact('labels'));
    }

    /**
     * @param Label $label
     * @return JsonResponse
     */
    public function delete(Label $label)
    {
        $label->delete();

        return response()->json(true);
    }
}
