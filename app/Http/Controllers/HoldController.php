<?php

namespace App\Http\Controllers;

use App\Exceptions\StatisticServiceException;
use App\Http\Controllers\Helpers\FilterTrait;
use App\Http\Controllers\Helpers\SitesTrait;
use App\Models\Label;
use App\Models\Responsible;
use App\Models\Role;
use App\Models\Sites;
use App\Services\MegaIndexService;
use App\Services\SeprSearchService;
use App\Services\StatisticService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class HoldController
 * @package App\Http\Controllers
 */
class HoldController extends Controller
{
    use SitesTrait,
        FilterTrait
    ;

    /**
     * @return View
     */
    public function index()
    {
        return view('hold.index');
    }
}
