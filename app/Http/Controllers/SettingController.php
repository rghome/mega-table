<?php

namespace App\Http\Controllers;

use App\Exceptions\StatisticServiceException;
use App\Http\Controllers\Helpers\FilterTrait;
use App\Http\Controllers\Helpers\SitesTrait;
use App\Models\Label;
use App\Models\Responsible;
use App\Models\Role;
use App\Models\Settings;
use App\Models\Sites;
use App\Services\MegaIndexService;
use App\Services\SeprSearchService;
use App\Services\StatisticService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class SettingController
 * @package App\Http\Controllers
 */
class SettingController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        return view('setting.index');
    }

    /**
     * @return JsonResponse
     */
    public function getSettingsList()
    {
        $settings = Settings::all();

        return response()->json(compact('settings'));
    }

    /**
     * @param Settings|null $settings
     * @return View
     */
    public function getCreateForm(Settings $settings = null)
    {
        return view('setting.create', compact('settings'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $setting = Settings::create($request->all());

        $settings = Settings::where('id', $setting->id)->first();

        return response()->json(compact('settings'));
    }

    /**
     * @param Request $request
     * @param Settings $settings
     * @return JsonResponse
     */
    public function update(Request $request, Settings $settings)
    {
//        $labels->update();

        $data = $request->all();

        $settings = Settings::where('id', $data['id'])->first();

        $settings->key = $data['key'];
        $settings->value = $data['value'];
        $settings->save();

        return response()->json(compact('settings'));
    }

    /**
     * @param Settings $settings
     * @return JsonResponse
     */
    public function delete(Settings $settings)
    {
        $settings->delete();

        return response()->json(true);
    }
}
