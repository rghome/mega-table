<?php

namespace App\Http\Controllers;

use App\Exceptions\SSLimitException;
use App\Exceptions\StatisticServiceException;
use App\Http\Controllers\Helpers\FilterTrait;
use App\Http\Controllers\Helpers\SitesTrait;
use App\Models\Responsible;
use App\Models\Sites;
use App\Services\StatisticService;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Database\Eloquent\Collection;

use Maatwebsite\Excel\Readers\LaravelExcelReader;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class StatisticController
 * @package App\Http\Controllers
 */
class StatisticController extends Controller
{
    use SitesTrait,
        FilterTrait
    ;

    /**
     * @return View
     */
    public function index()
    {
        return view('statistic.index');
    }

    /**
     * @return JsonResponse
     */
    public function getSitesList()
    {
        $user = Auth::user();

//        $cacheKey = 'sites-list-with-stat-' . $user->id;
        $sites = Sites::with('responsibleSeo')
            ->with('responsibleWM')
//            ->with([
//                'statistics' => function($query) {
//                    /** @var HasMany $query */
//
//                    $site = Sites::where('id', $query->getResults()->get(0)->site)->first();
//
//                    $query->where('engine', '=', $site->engine);
//                },
//            ])
            ->with('statistics')
            ->with('siteLabel')
            ->where('enabled', '1')
            ->orderBy('label', 'asc')
        ;

        if ($user->hasRole('webmaster')) {
            $sites = $sites->where('responsible_wm', $user->id);
        } else if ($user->hasRole('seo')) {
            $sites = $sites->where('responsible_seo', $user->id);
        }

        $sites = $sites->get();

        $sites = $this->sortingLabels($sites);

        $monthsStart = $this->monthsStart;

        return response()->json(compact('sites', 'monthsStart'));
    }

    /**
     * @param Request $request
     */
    public function download(Request $request)
    {
        $data = $this->doFilter($request);

        Excel::create('New report - ' . time(), function($excel) use ($data) {
            $excel->sheet('Главная', function($sheet) use ($data) {
                $sheet->row(1, ['URL', 'Трафик', '', '', 'Видимость', '', '', 'Ключевые фразу (в Топ10)']);
                $sheet->row(2, ['', 'текущий месяц', 'прошлый месяц', 'позапрошлый месяц', 'текущий месяц', 'прошлый месяц', 'позапрошлый месяц', 'текущий месяц', 'прошлый месяц', 'позапрошлый месяц']);

                $rows = [];
                $startRowIndex = 3;
                $labelRows = [];

                foreach ($data as $item) {
                    if (isset($item['label_name'])) {
                        $labelRows[] = $startRowIndex;

                        $rows[] = [$item['label_name'], '',  '', '', '', '', '', '', '', ''];
                    } else {
                        $currentStatistic = $this->filterMonth('current', $item->statistics);
                        $lastStatistic = $this->filterMonth('last', $item->statistics);
                        $previousStatistic = $this->filterMonth('previous', $item->statistics);

                        $rows[] = [
                            $item->url,
                            $currentStatistic['traff'],  $lastStatistic['traff'], $previousStatistic['traff'],
                            $currentStatistic['visible'],  $lastStatistic['visible'], $previousStatistic['visible'],
                            $currentStatistic['keywords'],  $lastStatistic['keywords'], $previousStatistic['keywords'],
                        ];
                    }
                }

                $sheet->rows($rows);
                $sheet->setMergeColumn(array(
                    'columns' => array('A'),
                    'rows'    => array(
                        array(1,2)
                    )
                ));

                foreach ($labelRows as $row) {
                    $sheet->mergeCells('A' . $row . ':J' . $row);
                }

                $sheet->mergeCells('B1:D1');
                $sheet->mergeCells('E1:G1');
                $sheet->mergeCells('H1:J1');

                $sheet->cells('A1:J1', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('middle');
                });

                //$sheet->setAlignment('center');


            });

            $excel->sheet('Ключевые фразы', function($sheet) use ($data) {
                $sheet->row(1, ['URL', 'Общее кол-во', 'Новые', 'Потерянные', 'Выросшие']);

                $rows = [];
                $startRowIndex = 2;
                $labelRows = [];

                foreach ($data as $item) {
                    if (isset($item['label_name'])) {
                        $labelRows[] = $startRowIndex;

                        $rows[] = [$item['label_name'], '',  '', '', ''];
                    } else {
                        $currentStatistic = $this->filterMonth('current', $item->statistics);

                        $rows[] = [
                            $item->url,
                            $currentStatistic['keywords'], $currentStatistic['new_keywords'],
                            $currentStatistic['out_keywords'], $currentStatistic['rised_keywords'],
                        ];
                    }
                }

                $sheet->rows($rows);

                foreach ($labelRows as $row) {
                    $sheet->mergeCells('A' . $row . ':E' . $row);
                }
            });

            $excel->sheet('Топ', function($sheet) use ($data) {
                $sheet->row(1, ['URL', 'Топ 1', 'Топ 2-3', 'Топ 4-5', 'Топ 6-10', 'Топ 11-20', 'Топ 21-50', '', 'Топ 3', 'Топ 5', 'Топ 20', 'Топ 50']);

                $rows = [];
                $startRowIndex = 2;
                $labelRows = [];

                foreach ($data as $item) {
                    if (isset($item['label_name'])) {
                        $labelRows[] = $startRowIndex;

                        $rows[] = [$item['label_name'], '',  '', '', '', '', '', '', '', '', '', ''];
                    } else {
                        $currentStatistic = $this->filterMonth('current', $item->statistics);

                        $rows[] = [
                            $item->url,
                            $currentStatistic['top1'], $currentStatistic['top23'], $currentStatistic['top45'],
                            $currentStatistic['top610'], $currentStatistic['top1120'], $currentStatistic['top2150'], '',
                            $currentStatistic['top3'], $currentStatistic['top5'], $currentStatistic['top20'],
                            $currentStatistic['top50'],
                        ];
                    }
                }

                $sheet->rows($rows);

                foreach ($labelRows as $row) {
                    $sheet->mergeCells('A' . $row . ':L' . $row);
                }
            });

            $excel->sheet('Видимость', function($sheet) use ($data) {
                $sheet->row(1, ['URL', 'Видимость', '', '']);
                $sheet->row(2, ['', 'текущий месяц', 'прошлый месяц', 'позапрошлый месяц']);

                $rows = [];
                $startRowIndex = 3;
                $labelRows = [];

                foreach ($data as $item) {
                    if (isset($item['label_name'])) {
                        $labelRows[] = $startRowIndex;

                        $rows[] = [$item['label_name'], '',  '', '', ];
                    } else {
                        $currentStatistic = $this->filterMonth('current', $item->statistics);
                        $lastStatistic = $this->filterMonth('last', $item->statistics);
                        $previousStatistic = $this->filterMonth('previous', $item->statistics);

                        $rows[] = [
                            $item->url,
                            $currentStatistic['visible'],  $lastStatistic['visible'], $previousStatistic['visible'],
                        ];
                    }
                }

                $sheet->rows($rows);
                $sheet->setMergeColumn(array(
                    'columns' => array('A'),
                    'rows'    => array(
                        array(1,2)
                    )
                ));

                foreach ($labelRows as $row) {
                    $sheet->mergeCells('A' . $row . ':D' . $row);
                }

                $sheet->mergeCells('B1:D1');
            });

            $excel->sheet('Трафик', function($sheet) use ($data) {
                $sheet->row(1, ['URL', 'Видимость', '', '']);
                $sheet->row(2, ['', 'текущий месяц', 'прошлый месяц', 'позапрошлый месяц']);

                $rows = [];
                $startRowIndex = 3;
                $labelRows = [];

                foreach ($data as $item) {
                    if (isset($item['label_name'])) {
                        $labelRows[] = $startRowIndex;

                        $rows[] = [$item['label_name'], '',  '', '', ];
                    } else {
                        $currentStatistic = $this->filterMonth('current', $item->statistics);
                        $lastStatistic = $this->filterMonth('last', $item->statistics);
                        $previousStatistic = $this->filterMonth('previous', $item->statistics);

                        $rows[] = [
                            $item->url,
                            $currentStatistic['traff'],  $lastStatistic['traff'], $previousStatistic['traff'],
                        ];
                    }
                }

                $sheet->rows($rows);
                $sheet->setMergeColumn(array(
                    'columns' => array('A'),
                    'rows'    => array(
                        array(1,2)
                    )
                ));

                foreach ($labelRows as $row) {
                    $sheet->mergeCells('A' . $row . ':D' . $row);
                }

                $sheet->mergeCells('B1:D1');
            });

            $excel->sheet('Ссылки', function($sheet) use ($data) {
                $sheet->row(1, ['URL', 'Ссыл. доменов', 'Ссыл. страниц', 'Проиндексировано']);

                $rows = [];
                $startRowIndex = 2;
                $labelRows = [];

                foreach ($data as $item) {
                    if (isset($item['label_name'])) {
                        $labelRows[] = $startRowIndex;

                        $rows[] = [$item['label_name'], '',  '', ''];
                    } else {
                        $currentStatistic = $this->filterMonth('current', $item->statistics);

                        $rows[] = [
                            $item->url,
                            $currentStatistic['links_domain'], $currentStatistic['links_pages'],
                            $currentStatistic['links_index'],
                        ];
                    }
                }

                $sheet->rows($rows);

                foreach ($labelRows as $row) {
                    $sheet->mergeCells('A' . $row . ':D' . $row);
                }
            });

            $excel->sheet('Ссылки (динамика)', function($sheet) use ($data) {
                $sheet->row(1, ['URL', 'текущий месяц', 'прошлый месяц', 'позапрошлый месяц']);

                $rows = [];
                $startRowIndex = 2;
                $labelRows = [];

                foreach ($data as $item) {
                    if (isset($item['label_name'])) {
                        $labelRows[] = $startRowIndex;

                        $rows[] = [$item['label_name'], '',  '', ''];
                    } else {
                        $currentStatistic = $this->filterMonth('current', $item->statistics);
                        $lastStatistic = $this->filterMonth('last', $item->statistics);
                        $previousStatistic = $this->filterMonth('previous', $item->statistics);

                        $rows[] = [
                            $item->url,
                            $currentStatistic['links_pages'],  $lastStatistic['links_pages'], $previousStatistic['links_pages']
                        ];
                    }
                }

                $sheet->rows($rows);

                foreach ($labelRows as $row) {
                    $sheet->mergeCells('A' . $row . ':D' . $row);
                }

            });
        })->download('xls');
    }

    /**
     * @param string $month
     * @param Collection $collection
     * @return array
     */
    private function filterMonth(string $month, Collection $collection)
    {
        $collection = $collection->toArray();

        $data = array_filter($collection, function ($value) use ($month) {
            return $value['month'] == $this->monthsStart[$month];
        });

        $data = reset($data);

        $result = [
            'traff' => (isset($data['traff'])) ? $data['traff'] : 0,
            'visible' => (isset($data['visible'])) ? $data['visible'] : 0,
            'keywords' => (isset($data['keywords'])) ? $data['keywords'] : 0,
            'new_keywords' => (isset($data['new_keywords'])) ? $data['new_keywords'] : 0,
            'out_keywords' => (isset($data['out_keywords'])) ? $data['out_keywords'] : 0,
            'rised_keywords' => (isset($data['rised_keywords'])) ? $data['rised_keywords'] : 0,
            'links_domain' => (isset($data['links_domain'])) ? $data['links_domain'] : 0,
            'links_pages' => (isset($data['links_pages'])) ? $data['links_pages'] : 0,
            'links_index' => (isset($data['links_index'])) ? $data['links_index'] : 0,
        ];

        $result['top1'] = (isset($data['top1'])) ? $data['top1'] : 0;
        $result['top23'] = (isset($data['top23'])) ? $data['top23'] : 0;
        $result['top45'] = (isset($data['top45'])) ? $data['top45'] : 0;
        $result['top610'] = (isset($data['top610'])) ? $data['top610'] : 0;
        $result['top1120'] = (isset($data['top1120'])) ? $data['top1120'] : 0;
        $result['top2150'] = (isset($data['top2150'])) ? $data['top2150'] : 0;
        $result['top3'] = (isset($data['top3'])) ? $data['top3'] : 0;
        $result['top5'] = (isset($data['top5'])) ? $data['top5'] : 0;
        $result['top20'] = (isset($data['top20'])) ? $data['top20'] : 0;
        $result['top50'] = (isset($data['top50'])) ? $data['top50'] : 0;

        return $result;
    }
}
