<?php

namespace App\Http\Controllers;

use App\Exceptions\StatisticServiceException;
use App\Http\Controllers\Helpers\FilterTrait;
use App\Http\Controllers\Helpers\SitesTrait;
use App\Models\Label;
use App\Models\Responsible;
use App\Models\Role;
use App\Models\Sites;
use App\Services\MegaIndexService;
use App\Services\SeprSearchService;
use App\Services\StatisticService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class SitesController
 * @package App\Http\Controllers
 */
class SitesController extends Controller
{
    use SitesTrait,
        FilterTrait
    ;

    /**
     * @return View
     */
    public function index()
    {
        return view('sites.index');
    }

    /**
     * @param Sites $sites
     * @return View
     */
    public function entry(Sites $sites)
    {
        return view('sites.entry', compact('sites'));
    }

    /**
     * @param Sites $sites
     * @param Request $request
     * @return JsonResponse
     */
    public function refreshSite(Sites $sites, Request $request)
    {
        $statisticService = new StatisticService();
        $statisticService->updateStatistic($sites);

        return $this->getSite($sites, $request);
    }

    /**
     * @param Sites $sites
     * @param Request $request
     * @return JsonResponse
     */
    public function getSite(Sites $sites, Request $request)
    {
        $engine = ($request->method() === 'POST') ? $request->get('engine') : $sites->engine;

//        try {
//            $this->statisticService->updateStatistic($sites);
//        } catch (StatisticServiceException $e) {}

        $site = Sites::where('id', $sites->id)
            ->with('responsibleSeo')
            ->with('responsibleWM')
            ->with('isMirror')
            ->with('siteLabel')
            ->where('enabled', '1')
            ->with([
                'statistics' => function($query) use ($engine) {
                    $query->where('engine', '=', $engine);
                },
                'getLinks' => function($query) use ($engine) {
                    $query->where('engine', '=', $engine);
                },
            ])
            ->first()
        ;

        $service = (new StatisticService())->setSite($site);
        $provider = $service->getService($engine);

        $typeClass = ($provider instanceof SeprSearchService) ? 'ss' : 'mi';

        $monthsStart = $this->monthsStart;

        $cacheName = 'site_dynamic_' . $site->id . '_' . $site->region;
        $dynamics = \Cache::rememberForever($cacheName, function() use ($service, $provider) {
            return $service->getDynamic($provider);
        });

        return response()->json(compact('site', 'monthsStart', 'typeClass', 'dynamics'));
    }

    /**
     * @param Sites|null $sites
     * @return View
     */
    public function getCreateForm(Sites $sites = null)
    {
        $responsibleSeo = Role::where('id', 8)->with('users')->first();
        $responsibleWM = Role::where('id', 7)->with('users')->first();
        $labels = Label::all();
        $sitesAll = Sites::where('id', '!=', $sites->id)->get();
        $regions = SeprSearchService::REGIONS + MegaIndexService::REGIONS;

        return view('sites.create', compact('sites', 'responsibleSeo', 'responsibleWM', 'labels', 'sitesAll', 'regions'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $sites = Sites::create($this->prepareData($request));

        try {
            (new StatisticService())->updateStatistic($sites);
        } catch (StatisticServiceException $e) {}

        $sites = Sites::where('id', $sites->id)
            ->with('responsibleSeo')
            ->with('responsibleWM')
            ->first()
        ;

        return response()->json(compact('sites'));
    }

    /**
     * @param Request $request
     * @param Sites $sites
     * @return JsonResponse
     */
    public function update(Request $request, Sites $sites)
    {

        $sites->update($this->prepareData($request));

//        try {
//            (new StatisticService())->updateStatistic($sites);
//        } catch (StatisticServiceException $e) {}

        (new StatisticService())->updateStatistic($sites);

        $sites = Sites::where('id', $sites->id)
            ->with('responsibleSeo')
            ->with('responsibleWM')
            ->first()
        ;

        return response()->json(compact('sites'));
    }

    /**
     * @param Sites $sites
     * @return JsonResponse
     */
    public function delete(Sites $sites)
    {
        $sites->delete();

        return response()->json(true);
    }

    /**
     * @param Request $request
     * @return array
     */
    private function prepareData(Request $request)
    {
        $data = $request->all();

        if ($data['mirror'] == '0') {
            $data['mirror'] = null;
        }

        return $data;
    }
}
