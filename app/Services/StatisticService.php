<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/7/17
 * Time: 10:56 AM
 */

namespace App\Services;

use App\Exceptions\SSLimitException;
use App\Models\Links;
use App\Models\Sites;
use App\Models\Statistics;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Serpstat\Sdk\Core\ApiGuzzleHttpClient;
use Serpstat\Sdk\Core\ApiResponse;
use Serpstat\Sdk\Interfaces\IApiClient;
use Serpstat\Sdk\Methods\DomainInfoMethod;

use Illuminate\Support\Facades\Log;

/**
 * Class StatisticService
 * @package App\Services
 */
class StatisticService
{
    /**
     * @var SeprSearchService
     */
    private $serpSearchService;

    /**
     * @var MegaIndexService
     */
    private $megaIndexService;

    /**
     * @var int
     */
    private $blockingMinutes = 15;

    /**
     * @var Sites
     */
    private $site;

    /**
     * @var array
     */
    private $engineMap = [
        'yandex' => [
            IApiClient::SE_YANDEX_SPB,
            IApiClient::SE_YANDEX_MSK,
            135, 58, 130, 74, 230, 144,
            173, 165, 220, 96, 152, 41,
            36, 81,
        ],
        'google' => [
            IApiClient::SE_GOOGLE_RU,
            2926, 2846, 3019
        ]
    ];

    private $defaultEngine = IApiClient::SE_GOOGLE_RU;

    /**
     * StatisticService constructor.
     */
    public function __construct()
    {
        $this->serpSearchService = new SeprSearchService();
        $this->megaIndexService = new MegaIndexService();
    }

    /**
     * @param AggregatorInterface $service
     * @return array
     */
    public function getDynamic(AggregatorInterface $service)
    {
        if ($service instanceof MegaIndexService) {
            $data = $this->serpSearchService->getDomainHistory($this->site, 'g_ru');
        } else {
            $data = $this->serpSearchService->getDomainHistory($this->site, $this->site->region);
        }

        $data = array_reverse($data);

        foreach ($data as $index => $item) {
            if (isset($data[$index + 1])) {
                foreach ($item as $key => $value) {
                    if ($value > $data[$index + 1][$key]) {
                        $data[$index][$key . '_status'] = 'up';
                    } elseif ($value < $data[$index + 1][$key]) {
                        $data[$index][$key . '_status'] = 'down';
                    }
                }
            }
        }

//        if (isset($data[0]) && isset($data[1])) {
//            foreach ($data[0] as $key => $value) {
//                if ($value > $data[1][$key]) {
//                    $data[0][$key . '_status'] = 'up';
//                } elseif ($value < $data[1][$key]) {
//                    $data[0][$key . '_status'] = 'down';
//                }
//            }
//        }

        return $data;
    }

    /**
     * Update statistic with 15 minutes blocking
     *
     * @param Sites|null $sites
     */
    public function updateStatistic(Sites $sites = null)
    {
        if ($sites instanceof Sites) {
            $sites = Sites::where('id', $sites->id)
                ->with('statistics')
                ->get()
            ;
        } else {
            $sites = Sites::with('statistics')->get();
        }

        /** @var Sites $site */
        foreach ($sites as $site) {
            $this->site = $site;

            $this->updateStatisticSite();
        }
    }

    /**
     * Fill statistic
     */
    private function updateStatisticSite()
    {
        if ($this->site->statistics->isNotEmpty()) {
            $currentRow = $this->filterCurrentStatistic();

            if (isset($currentRow[0])) {
                $this->updateCurrentStatistic($currentRow);
            } else {
                $this->createStatistic();
            }
        } else {
            $this->createStatistic();
        }
    }

    /**
     * Create new statistic rows
     */
    private function createStatistic()
    {
        foreach (array_keys($this->engineMap) as $engine) {
            $fromApi = $this->parseFromApiDomainInfo($engine);

            $this->refreshLinks($fromApi);

            Statistics::create($fromApi);
        }
    }

    /**
     * @param array $currentRow
     */
    private function updateCurrentStatistic(array $currentRow)
    {
        $createdAt = Carbon::parse($currentRow[0]['updated_at']);

//                if (Carbon::now()->diffInMinutes($createdAt) <= $this->blockingMinutes) {
//                    return;
//                }

        foreach (array_keys($this->engineMap) as $engine) {
            $fromApi = $this->parseFromApiDomainInfo($engine);

            $this->refreshLinks($fromApi);

            $statisticRow = Statistics::where('site', $fromApi['site'])
                ->where('month', $currentRow[0]['month'])
                ->where('engine', $fromApi['engine'])
                ->first()
            ;

            if ($statisticRow === null) {
                Statistics::create($fromApi);
            } else {
                $statisticRow->update($fromApi);
            }
        }
    }

    /**
     * @param array $data
     */
    private function refreshLinks(array &$data)
    {
        $links = $data['links'];
        unset($data['links']);

        Links::where('site_id', $data['site'])
            ->where('engine', $data['engine'])
            ->delete()
        ;

        foreach ($links as $key => $link) {
            $link['keywords'] = (int) str_replace(' ', '', $link['keywords']);
            $links[$key] = $link;
        }

        \DB::table('links')->insert($links);
    }

    /**
     * @return array
     */
    private function filterCurrentStatistic()
    {
        return array_filter($this->site->statistics->toArray(), function($row) {
            return $row['month'] === $this->getMonthStart();
        });
    }

    /**
     * @param string $engine
     * @return mixed
     */
    private function parseFromApiDomainInfo(string $engine)
    {
        $this->flushCache();

        $service = $this->getService($engine);

        $result = $service->getDomainInfo($this->site);

        $result['engine'] = $engine;
        $result['month'] = $this->getMonthStart();
        $result['site'] = $this->site->id;
        $result['links'] = $service->getTopLinks($engine);

        return $result;
    }

    /**
     * @param Sites $sites
     * @return $this
     */
    public function setSite(Sites $sites)
    {
        $this->site = $sites;

        return $this;
    }

    /**
     * @param string $engine
     * @return MegaIndexService|SeprSearchService
     */
    public function getService(string $engine)
    {
        if ($engine == $this->site->engine) {
            return $this->checkService();
        } else {
             if (in_array($this->site->region, $this->engineMap[$engine])) {
                 return $this->checkService();
             } else {
                 $this->site->region = $this->defaultEngine;

                 return $this->serpSearchService;
             }
        }
    }

    /**
     * @return MegaIndexService|SeprSearchService
     */
    private function checkService()
    {
        try {
            SeprSearchService::REGIONS[$this->site->region];

            return $this->serpSearchService;
        } catch (\Exception $e) {
            return $this->megaIndexService;
        }
    }

    /**
     * @return array
     */
    public function getMonthsStart()
    {
        return [
            'current' => $this->getMonthStart(),
            'last' => $this->getMonthStart(1),
            'previous' => $this->getMonthStart(2)
        ];
    }

    /**
     * @param int|null $prev
     * @return int
     */
    private function getMonthStart(int $prev = null)
    {
        if ($prev !== null) {
            return Carbon::now()->startOfMonth()->subMonth($prev)->timestamp;
        }

        return Carbon::now()->startOfMonth()->timestamp;
    }

    /**
     * @return bool
     */
    private function flushCache()
    {
        \Cache::flush();

        return true;
    }
}