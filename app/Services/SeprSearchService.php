<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/10/17
 * Time: 11:39 PM
 */

namespace App\Services;

use App\Exceptions\SSLimitException;
use App\Models\Sites;
use App\Models\Settings;
use App\Models\Statistics;
use Carbon\Carbon;
use DiDom\Element;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Database\Eloquent\Collection;
use Serpstat\Sdk\Core\ApiGuzzleHttpClient;
use Serpstat\Sdk\Core\ApiResponse;
use Serpstat\Sdk\Interfaces\IApiClient;
use Serpstat\Sdk\Methods\DomainHistoryMethod;
use Serpstat\Sdk\Methods\DomainInfoMethod;
use Illuminate\Support\Facades\Log;
use DiDom\Document;

use GuzzleHttp\Client;

/**
 * Class SeprSearchService
 * @package App\Services
 */
class SeprSearchService implements AggregatorInterface
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var ApiGuzzleHttpClient
     */
    private $client;

    /**
     * @var Client
     */
    private $nativeClient;

    /**
     * @var Document
     */
    private $document;

    /**
     * @var Sites
     */
    private $site;

    /**
     * List of regions
     */
    const REGIONS = [
        IApiClient::SE_GOOGLE_RU => 'CC: Google Россия',
        IApiClient::SE_YANDEX_MSK => 'CC: Yandex Москва',
        IApiClient::SE_YANDEX_SPB => 'CC: Yandex Санкт-Петербург'
    ];

    /**
     * StatisticService constructor.
     */
    public function __construct()
    {
        $this->token = Settings::where('key', 'serpStatToken')->first()->value ?: 'dc69e9fd0c48a42139bf3b1c115b8425';

        $this->client = new ApiGuzzleHttpClient($this->token);
        $this->nativeClient = new Client(['cookies' => true]);
        $this->document = Document::class;
    }

    /**
     * @param Sites $site
     * @param string $region
     * @return array
     */
    public function getDomainHistory(Sites $site, string $region)
    {
        $apiMethod = new DomainHistoryMethod($site->url, $region);
        try {
            return $this->client->call($apiMethod)->getResult();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * @param Sites $site
     * @return ApiResponse|array
     * @throws SSLimitException
     */
    public function getDomainInfo(Sites $site)
    {
        $this->site = $site;

        $apiMethod = new DomainInfoMethod($site->url, $site->region);

        try {
            /** @var ApiResponse $result */
            $result = $this->client->call($apiMethod)->getResult();
            $result = $this->login()->getKeywordsData() + $result + $this->login()->getLinksData();

            $result['top1'] = $this->login()->getKeywordsData(1, 1)['keywords'];
            $result['top23'] = $this->login()->getKeywordsData(2, 3)['keywords'];
            $result['top45'] = $this->login()->getKeywordsData(4, 5)['keywords'];
            $result['top610'] = $this->login()->getKeywordsData(6, 10)['keywords'];
            $result['top1120'] = $this->login()->getKeywordsData(11, 20)['keywords'];
            $result['top2150'] = $this->login()->getKeywordsData(21, 50)['keywords'];
            $result['top3'] = $result['top1'] + $result['top23'];
            $result['top5'] = $result['top3'] + $result['top45'];
            $result['top10'] = $result['top5'] + $result['top610'];
            $result['top20'] = $result['top10'] + $result['top1120'];
            $result['top50'] = $result['top20'] + $result['top2150'];
            $result['site'] = $site->id;

            return $result;
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return [
                'site' => $site->id,
                'visible' => 0,
                'traff' => 0,
                'keywords' => 0,
                'new_keywords' => 0,
                'out_keywords' => 0,
                'rised_keywords' => 0,
                'top1' => 0,
                'top23' => 0,
                'top45' => 0,
                'top610' => 0,
                'top1120' => 0,
                'top2150' => 0,
                'top3' => 0,
                'top5' => 0,
                'top10' => 0,
                'top20' => 0,
                'top50' => 0,
                'visible_dynamic' => 0,
                'keywords_dynamic' => 0,
                'traff_dynamic' => 0,
                'ads_dynamic' => 0,
                'down_keywords' => 0,
                'ad_keywords' => 0,
                'ads' => 0,
                'links_domain' => 0,
                'links_pages' => 0,
                'links_index' => 0
            ];

//            throw new SSLimitException('SS: ' . $e->getMessage() . '; Query String: ' . $apiMethod->getUrlQueryString());
        }
    }

    /**
     * @param string $engine
     * @return array
     */
    public function getTopLinks(string $engine)
    {
        return $this->login()->parseTop15($engine);
    }


    /**
     * @param Sites|null $sites
     * @return $this
     */
    public function login(Sites $sites = null)
    {
        if (!$this->site) {
            $this->site = $sites;
        }

        /** @var CookieJar $jar */
        $jar = $this->nativeClient->getConfig('cookies');

        if (!$jar->count()) {
            $this->nativeClient->post('https://serpstat.com/users/login/', [
                'host' => 'serpstat.com',
                'user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
                'origin ' => 'https://serpstat.com',
                'referer' => 'https://serpstat.com/domains/search/?query=' . $this->site->url . '&se=' . $this->site->region . '&search_type=subdomains&dynamic=&dynamic_from_date=&filter:0:0:block_condition=and&filter:0:0:field=position&filter:0:0:condition=between&filter:0:0:value_from=1&filter:0:0:value_to=10',
                'content-type' => 'application/x-www-form-urlencoded; charset=UTF-8',
                'form_params' => [
                    'email' => 'onufriy47@gmail.com',
                    'pass' => 'gfnfvexnfukflbjkecs'
                ],
            ]);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getLinksData()
    {
        $response = $this->nativeClient->get('https://serpstat.com/backlink/dashboard/?query=' . $this->site->url);
        $content = $response->getBody()->getContents();

        $document = new Document($content);
        $rows = $document->find('.mr5');

        return [
            'links_domain' => str_replace('.', '', trim((strpos($rows[0]->text(), 'K')) ? substr(trim($rows[0]->text()), 0, -1) . '00' : $rows[0]->text())),
            'links_pages' => str_replace('.', '', trim((strpos($rows[1]->text(), 'K')) ? substr(trim($rows[1]->text()), 0, -1) . '00' : $rows[1]->text())),
            'links_index' => str_replace('.', '', trim((strpos($rows[2]->text(), 'K')) ? substr(trim($rows[2]->text()), 0, -1) . '00' : $rows[2]->text())),
        ];
    }

    /**
     * @param string $engine
     * @param int $page
     * @param array $result
     * @return array
     */
    private function parseTop15(string $engine, int $page = 1, array $result = [])
    {
        $response = $this->nativeClient->get('https://serpstat.com/domains/urls/?query=' . $this->site->url . '&se=' . $this->site->region . '&page=' . $page);
        $content = $response->getBody()->getContents();

        $document = new Document($content);
        $rows = $document->find('tr');

        $paginator = $document->find('.pagination');

        foreach ($rows as $key => $row) {
            if ($key === 0) continue;

            $keywords = trim($row->child(5)->text()) ?: null;

            if (!$keywords) {
                continue;
            }

            $url = trim($row->child(3)->text());

            $result[] = [
                'url' => $url,
                'engine' => $engine,
                'site_id' => $this->site->id,
                'keywords' => $keywords
            ];
        }

        if ($paginator) {
            $lastPage = (int) trim($paginator[0]->lastChild()->text());

            if ($page !== $lastPage) {
                return $this->parseTop15($engine, $page + 1, $result);
            }
        }

        return $result;
    }

    /**
     * @param int $from
     * @param int $to
     * @param int $page
     * @param int $countKeywords
     * @param int $countNewKeywords
     * @param int $countRiseKeywords
     * @param int $countOutKeywords
     * @return array
     */
    public function getKeywordsData(
        int $from = 1, int $to = 10,
        int $page = 1, int $countKeywords = 0, int $countNewKeywords = 0,
        int $countRiseKeywords = 0, $countOutKeywords = 0
    )
    {
        $url = 'https://serpstat.com/domains/search/?query=' . $this->site->url . '&se=' . $this->site->region . '&search_type=subdomains&dynamic=&dynamic_from_date=&filter:0:0:block_condition=and&filter:0:0:field=position&filter:0:0:condition=between&filter:0:0:value_from=' . $from . '&filter:0:0:value_to=' . $to . '&page=' . $page;

        $response = $this->nativeClient->get($url);
        $content = $response->getBody()->getContents();

        $document = new Document($content);
        $rows = $document->find('tr');

        $paginator = $document->find('.pagination');

        foreach ($rows as $key => $row) {
            if ($key === 0) continue;

            $position = $row->child(5)->find('.tr');

            if (!$position) {
                continue;
            }

            $rise = $row->child(5)->find('.color_green');
            $out = $row->child(5)->find('.color_red');

            if (!strpos($position[0]->text(), '*')) {
                if (strpos($row->text(), 'New')) {
                    $countNewKeywords++;
                    $countRiseKeywords--;
                }

                if (!empty($rise)) {
                    $countRiseKeywords++;
                }

                if (!empty($out)) {
                    $countOutKeywords++;
                }

                $countKeywords++;
            }
        }

        if ($paginator) {
            $lastPage = (int) trim($paginator[0]->lastChild()->text());

            if ($page !== $lastPage) {
                return $this->getKeywordsData(
                    $from, $to,
                    $page + 1, $countKeywords, $countNewKeywords,
                    $countRiseKeywords, $countOutKeywords
                );
            }
        }

        return [
            'keywords' => $countKeywords,
            'new_keywords' => $countNewKeywords,
            'out_keywords' => $countOutKeywords,
            'rised_keywords' => $countRiseKeywords
        ];
    }
}