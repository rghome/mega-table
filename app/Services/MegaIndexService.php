<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/10/17
 * Time: 11:39 PM
 */

namespace App\Services;

use App\Models\Settings;
use App\Models\Sites;
use GuzzleHttp\Client;
use DiDom\Document;

/**
 * Class MegaIndexService
 * @package App\Services
 */
class MegaIndexService implements AggregatorInterface
{
    /**
     * @var string
     */
    private $token;

    /**
     * List of regions
     */
    const REGIONS = [
        2846 => 'MI: Google Москва ',
        3019 => 'MI: Google Санкт-Петербург ',
        135 => 'MI: Yandex Новосибирск ',
        58 => 'MI: Yandex Екатеринбург ',
        130 => 'MI: Yandex Нижний Новгород ',
        74 => 'MI: Yandex Казань ',
        230 => 'MI: Yandex Челябинск ',
        144 => 'MI: Yandex Омск ',
        173 => 'MI: Yandex Самара ',
        165 => 'MI: Yandex Ростов-на-Дону ',
        220 => 'MI: Yandex Уфа ',
        96 => 'MI: Yandex Красноярск ',
        152 => 'MI: Yandex Пермь ',
        41 => 'MI: Yandex Воронеж ',
        36 => 'MI: Yandex Волгоград ',
        81 => 'MI: Yandex Киев ',
        2926 => 'MI: Google Киев ',
    ];

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Document
     */
    private $dom;

    /**
     * @var Sites
     */
    private $site;

    /**
     * MegaIndexService constructor.
     */
    public function __construct()
    {
        $this->dom = new Document();
        $this->client = new Client();

        $this->token = Settings::where('key', 'megaIndexToken')->first()->value ?: 'e680b42c8c11fecd93c8ef749d7f77f5';
    }

    /**
     * @param Sites $sites
     * @return array
     */
    public function getDomainInfo(Sites $sites)
    {
        $this->site = $sites;

        $url = 'https://ru.megaindex.com/visibility/' . $sites->url . '?ser_id=' . $sites->region;
        $document = new Document($url, true);

        $context1 = $document->find('.count');
        $context2 = explode(' ', trim(explode(PHP_EOL, trim($document->find('#ad table')[0]->text()))[2]))[0];
        $context3 = explode(' ', trim(explode(PHP_EOL, trim($document->find('#ad table')[0]->text()))[0]))[0];
        $context4 = explode(' ', trim(explode(PHP_EOL, trim($document->find('#ad table')[0]->text()))[4]))[0];
        $context5 = trim($document->find('#ad .count')[1]->text());

        $links = (new SeprSearchService())->login($sites)->getLinksData();

        return [
            'visible' => str_replace('.', '', trim((strpos($context1[0]->text(), 'K')) ? substr(trim($context1[0]->text()), 0, -1) . '00' : $context1[0]->text())),
            'traff' => str_replace('.', '', trim((strpos($context1[2]->text(), 'K')) ? substr(trim($context1[2]->text()), 0, -1) . '00' : $context1[2]->text())),
            'keywords' => $context2,
            'new_keywords' => 0,
            'out_keywords' => 0,
            'rised_keywords' => 0,
            'top1' => 0,
            'top23' => 0,
            'top45' => 0,
            'top610' => 0,
            'top1120' => 0,
            'top2150' => 0,
            'top3' => 0,
            'top5' => $context3,
            'top10' => $context2,
            'top20' => $context4,
            'top50' => $context5,
            'visible_dynamic' => 0,
            'keywords_dynamic' => 0,
            'traff_dynamic' => 0,
            'ads_dynamic' => 0,
            'down_keywords' => 0,
            'ad_keywords' => 0,
            'ads' => 0,
            'links_domain' => $links['links_domain'],
            'links_pages' => $links['links_pages'],
            'links_index' => $links['links_index']
        ];
    }

    /**
     * @param string $engine
     * @return array
     */
    public function getTopLinks(string $engine)
    {
        $url = 'http://api.megaindex.com/visrep/popular_url?key=' . $this->token . '&domain=' . $this->site->url . '&ser_id=' . $this->site->region . '&count=5000';

        if (\Cache::has('test_mi')) {
            $data = \Cache::get('test_mi');
        } else {
            $data = json_decode(file_get_contents($url), true)['data'];

            \Cache::forever('test_mi', $data);
        }

        $new_array = array();
        foreach ($data as $item) {
            $new_array[$item['path']][] = $item;
        }

        $result = [];
        foreach ($new_array as $path => $item) {
            $result[] = [
                'url' => $this->site->url . $path,
                'engine' => $engine,
                'site_id' => $this->site->id,
                'keywords' => count($item)
            ];
        }

        return $result;
    }
}