<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/11/17
 * Time: 10:51 PM
 */

namespace App\Services;


use App\Models\Sites;

/**
 * Interface AggregatorInterface
 * @package App\Services
 */
interface AggregatorInterface
{
    public function getDomainInfo(Sites $sites);
}