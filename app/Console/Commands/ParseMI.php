<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Yangqi\Htmldom\Htmldom;
use PHPHtmlParser\Dom;

/**
 * Class ParseMI
 * @package App\Console\Commands
 */
class ParseMI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse-mi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Dom
     */
    private $dom;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = new Client();

        $this->dom = new Dom();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $url = 'https://ru.megaindex.com/visibility/diplom-kupit-perm.ru?ser_id=152';
//
////        $body = (string) $this->client->get($url)->getBody();
//
//        $this->dom->loadFromUrl($url);
//
////        $body = $this->dom->outerHtml;
//
//        $context = $this->dom->find('.count');
//
//        /** @var Dom\HtmlNode $count */
//        foreach ($context as $count) {
//            echo $count->innerhtml . PHP_EOL;
//        }

        $url = 'http://api.megaindex.com/visrep/get_ser?key=e680b42c8c11fecd93c8ef749d7f77f5';

        $request = $this->client->request('get', $url);
        $data = json_decode($request->getBody()->getContents(), true);

        $countries = [
            'Россия' => [
                'key' => 1,
                'data' => $data['data'][1]
            ],
            'Украина' => [
                'key' => 261,
                'data' => $data['data'][261]
            ]
        ];

        $result = [];
        foreach ($countries as $country) {
            foreach ($country['data']['ser_ids'] as $item) {
                if (in_array((int) $item['ser_id'], [1, 174])) {
                    continue;
                }

                if (in_array((int) $item['se_id'], [1, 3])) {
                    $result[$item['ser_id']] = 'MI: ' . $item['se_name'] . ' ' . $item['name'];
                }
            }
        }

        $str = '[' . PHP_EOL;
        foreach ($result as $key => $value) {

            $str .= $key . ' => \'' . $value . ' \',' . PHP_EOL;
        }
        $str .= '];' . PHP_EOL;

        \Cache::forever('key', $result);
    }
}
