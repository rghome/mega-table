<?php

namespace App\Console\Commands;

use App\Models\Sites;
use App\Services\StatisticService;
use Illuminate\Console\Command;

class UpdateStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-statistics {site?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $siteId = $this->argument('site');
        $statisticService = new StatisticService();

        if ($siteId) {
            $statisticService->updateStatistic(Sites::where('id', $siteId)->first());
        } else {
            $statisticService->updateStatistic();
        }
    }
}
