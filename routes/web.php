<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'StatisticController@index')->name('statistic');

Route::get('/statistic/load', 'StatisticController@getSitesList')->name('statisticLoad');
Route::get('/statistic/download', 'StatisticController@download')->name('statisticDownload');
Route::post('/statistic/filter', 'StatisticController@filter')->name('statisticFilter');
Route::post('/statistic/filter/get-data', 'StatisticController@filterGetData')->name('statisticFilterGetData');

Route::get('/user', 'UserController@index')->name('user');
Route::get('/user/load', 'UserController@getUsersList')->name('userLoad');
Route::get('/user/create/getForm/{user?}', 'UserController@getCreateForm')->name('userCreateForm');
Route::post('/user/create', 'UserController@create')->name('userCreate');
Route::post('/user/update/{user}', 'UserController@update')->name('userUpdate');
Route::delete('/user/delete/{user}', 'UserController@delete')->name('userDelete');

Route::get('/sites', 'SitesController@index')->name('sites');
Route::get('/site/{sites?}', 'SitesController@entry')->name('site');
Route::any('/site/get/{sites?}', 'SitesController@getSite')->name('getSite');
Route::any('/site/refresh/{sites?}', 'SitesController@refreshSite')->name('refreshSite');
Route::get('/sites/load', 'SitesController@getSitesList')->name('sitesLoad');
Route::get('/sites/create/getForm/{sites?}', 'SitesController@getCreateForm')->name('sitesCreateForm');
Route::post('/sites/create', 'SitesController@create')->name('sitesCreate');
Route::post('/sites/update/{sites}', 'SitesController@update')->name('sitesUpdate');
Route::post('/sites/filter', 'SitesController@filter')->name('sitesFilter');
Route::post('/sites/filter/get-data', 'SitesController@filterGetData')->name('sitesFilterGetData');
Route::delete('/sites/delete/{sites}', 'SitesController@delete')->name('sitesDelete');

Route::get('/hold', 'HoldController@index')->name('hold');
Route::get('/hold/load', 'HoldController@getSitesList')->name('holdLoad');

Route::get('/setting', 'SettingController@index')->name('setting');
Route::get('/setting/load', 'SettingController@getSettingsList')->name('settingLoad');
Route::get('/setting/create/getForm/{settings?}', 'SettingController@getCreateForm')->name('settingCreateForm');
Route::post('/setting/create', 'SettingController@create')->name('settingCreate');
Route::post('/setting/update/{settings}', 'SettingController@update')->name('settingUpdate');
Route::delete('/setting/delete/{settings}', 'SettingController@delete')->name('settingDelete');

Route::get('/labels', 'LabelController@index')->name('label');
Route::get('/label/load', 'LabelController@getLabelsList')->name('labelLoad');
Route::get('/label/create/getForm/{label?}', 'LabelController@getCreateForm')->name('labelCreateForm');
Route::post('/label/create', 'LabelController@create')->name('labelCreate');
Route::post('/label/update/{label}', 'LabelController@update')->name('labelUpdate');
Route::delete('/label/delete/{label}', 'LabelController@delete')->name('labelDelete');

Route::get('/role', 'RoleController@index')->name('role');
Route::get('/role/load', 'RoleController@getRolesList')->name('roleLoad');
Route::get('/role/create/getForm/{role?}', 'RoleController@getCreateForm')->name('roleCreateForm');
Route::post('/role/create', 'RoleController@create')->name('roleCreate');
Route::post('/role/update/{role}', 'RoleController@update')->name('roleUpdate');
Route::delete('/role/delete/{role}', 'RoleController@delete')->name('roleDelete');